/* 
    Red-Black Tree implementation for a self balancing BST 
*/

"use strict";

const RED = "RED";
const BLACK = "BLACK";

let NIL = {
    // sentinel node, so that all leaves can be colored black by defalut.
    color: BLACK,
    val: "NIL",
    parent: null
};

exports.NIL = NIL;

const createNode = (val, left = NIL, right = NIL, parent = NIL, color = BLACK, count = 1) => {
    return {
        val: val,
        left: left,
        right: right,
        parent: parent,
        color: color,
        count: count
    };
};

exports.createNode = createNode;

const leftRotate = (root, node) => {
    let tmp = node.right;
    node.right = tmp.left;
    if (tmp.left !== NIL) {
        tmp.left.parent = node;
    }
    tmp.parent = node.parent;
    if (node.parent === NIL) {
        root = tmp;
    } else if (node.val === node.parent.left.val) {
        node.parent.left = tmp;
    } else {
        node.parent.right = tmp;
    }
    tmp.left = node;
    node.parent = tmp;
    node = tmp = null;
    return root;
};

exports.rightRotate = rightRotate;

const rightRotate = (root, node) => {
    let tmp = node.left;
    node.left = tmp.right;
    if (tmp.right !== NIL) {
        tmp.right.parent = node;
    }
    tmp.parent = node.parent;
    if (node.parent === NIL) {
        root = tmp;
    } else if (node.val === node.parent.right.val) {
        node.parent.right = tmp;
    } else {
        node.parent.left = tmp;
    }
    tmp.right = node;
    node.parent = tmp;
    node = tmp = null;
    return root;
};

exports.leftRotate = leftRotate;

const insertValue = (root, val) => {
    let parent = NIL;
    let tmp = root;
    let cloneParent = NIL;
    let cloneTmp = createNode(tmp.val, NIL, NIL, tmp.parent, tmp.color, tmp.count);
    let cloneRoot = cloneTmp;
    let tmpNode, found = false, foundAt = NIL;
    while (tmp !== NIL) {
        parent = tmp;
        cloneParent = cloneTmp;
        if (val === tmp.val) {
            found = true;
            foundAt = cloneTmp; // store pointer to the location the element was found at and increment it's value.
        }
        if (val < tmp.val) {
            if (tmp.right !== NIL) { // clone node, parent and uncle.
                tmpNode = createNode(tmp.right.val, NIL, NIL, tmp.right.parent, tmp.right.color, tmp.right.count);
                tmpNode.right = tmp.right.right;
                tmpNode.left = tmp.right.left;
                cloneParent.right = tmpNode;
            }
            else {
                cloneParent.right = tmp.right;
            }
            tmp = tmp.left;
            if (tmp != NIL) {
                cloneTmp = createNode(tmp.val, NIL, NIL, cloneParent, tmp.color, tmp.count);
                cloneParent.left = cloneTmp;
            }
              
        } else {
            if (tmp.left !== NIL) { // clone node, parent and uncle
                tmpNode = createNode(tmp.left.val, NIL, NIL, tmp.left.parent, tmp.left.color, tmp.left.count);
                tmpNode.right = tmp.left.right;
                tmpNode.left = tmp.left.left;
                cloneParent.left = tmpNode;
            } else {
                cloneParent.left = tmp.left;
            }
            tmp = tmp.right;
            if (tmp  !=  NIL) {
                cloneTmp = createNode(tmp.val, NIL, NIL, cloneParent, tmp.color, tmp.count);
                cloneParent.right = cloneTmp;
            }
        }
    }
    if (found === true) {
        // duplicate element: increment value of the clone node, do not add the new node to tree again.
        foundAt.count++;
    } else {
        let newNode = createNode(val);
        newNode.parent = cloneParent;
        if (parent === NIL) {
            cloneRoot = newNode;
        } else if (newNode.val < parent.val) {
            cloneParent.left = newNode;
        } else {
            cloneParent.right = newNode;
        }
        newNode.left = NIL;
        newNode.right = NIL;
        newNode.color = RED;
        cloneRoot = redBlackInsertFixup(cloneRoot, newNode);
    }
    // parent = tmp = root = cloneTmp = cloneParent = tmpNode = foundAt = null; // setting pointers to null to facilitate GC, since NIL node is global.
    return cloneRoot;
 };

exports.insertValue = insertValue;

const redBlackInsertFixup = (root, node) => {
    // maintain Red-black properties of the tree on a node insertion.
    let uncle;
    while (node.parent.color === RED) {
        if (node.parent.val === node.parent.parent.left.val) {
            uncle = node.parent.parent.right;
            if (uncle.color === RED) {
                node.parent.color = BLACK;
                uncle.color = BLACK;
                node.parent.parent.color = RED;
                node = node.parent.parent;
            } else {
                if (node.val === node.parent.right.val) {
                    node = node.parent;
                    root = leftRotate(root, node);
                }
                node.parent.color = BLACK;
                node.parent.parent.color = RED;
                root = rightRotate(root, node.parent.parent);
            }
        } else {
            uncle = node.parent.parent.left;
            if (uncle.color === RED) {
                node.parent.color = BLACK;
                uncle.color = BLACK;
                node.parent.parent.color = RED;
                node = node.parent.parent;
            } else {
                if (node.val === node.parent.left.val) {
                    node = node.parent;
                    root = rightRotate(root, node);
                }
                node.parent.color = BLACK;
                node.parent.parent.color = RED;
                root = leftRotate(root, node.parent.parent);
            }
        }
        uncle = null;
    }
    node = null;
    root.color = BLACK;
    return root;
};

exports.RBInsertFixup = redBlackInsertFixup;

const findSmallestDescendent = (node) => {
    // find the smallest descendent of node (useful for deletions)
    let tmp = node, self = null;
    while (tmp != NIL) {
        self = tmp;
        tmp = tmp.left;
    }
    return self;
};

exports.findSmallestDescendent = findSmallestDescendent;

const findValue = (root, val) => {
    // find a value in the Red-black tree. Return the node if found or false if not found
    let tmp = root;
    let parent = NIL, cloneParent = NIL;
    let cloneRoot = createNode(tmp.val, NIL, NIL, tmp.parent, tmp.color, tmp.count);
    let cloneTmp = cloneRoot;
    let tmpNode, found = false, foundAt = NIL;
    while (tmp !== NIL) {
        parent = tmp;
        cloneParent = cloneTmp;
        if (val === tmp.val) {
            found = true;
            foundAt = cloneTmp;
            foundAt.left = tmp.left;
            foundAt.right = tmp.right;
            break;
        } else if (val < tmp.val) {
            if (tmp.right !== NIL) { // clone node, parent and uncle.
                tmpNode = createNode(tmp.right.val, NIL, NIL, tmp.right.parent, tmp.right.color, tmp.right.count);
                tmpNode.right = tmp.right.right;
                tmpNode.left = tmp.right.left;
                cloneParent.right = tmpNode;
            }
            else {
                cloneParent.right = tmp.right;
            }
            tmp = tmp.left;
            if (tmp != NIL) {
                cloneTmp = createNode(tmp.val, NIL, NIL, cloneParent, tmp.color, tmp.count);
                cloneParent.left = cloneTmp;
            }
              
        } else {
            if (tmp.left !== NIL) { // clone node, parent and uncle
                tmpNode = createNode(tmp.left.val, NIL, NIL, tmp.left.parent, tmp.left.color, tmp.left.count);
                tmpNode.right = tmp.left.right;
                tmpNode.left = tmp.left.left;
                cloneParent.left = tmpNode;
            } else {
                cloneParent.left = tmp.left;
            }
            tmp = tmp.right;
            if (tmp  !=  NIL) {
                cloneTmp = createNode(tmp.val, NIL, NIL, cloneParent, tmp.color, tmp.count);
                cloneParent.right = cloneTmp;
            }
        }
    }
    root = cloneRoot;
    return {found, element: foundAt, root};
};

exports.findValue = findValue;

const redBlackTransplant = (root, u, v) => {
    if (u.parent === NIL) {
        root = v;
    } else if (u.val === u.parent.left.val) {
        u.parent.left = v;
    } else {
        u.parent.right = v;
    }
    v.parent = u.parent;
    return root;
};

const deleteValue = (root, val) => {
    let res = findValue(root, val);
    if (res.found && root && root != NIL) {
        root = res.root;
        val = res.element;
        let y = val, x;
        let yOriginalColor = y.color;
        if (val.left === NIL) {
            x = val.right;
            root = redBlackTransplant(root, val, val.right);
        } else if (val.right === NIL) {
            x = val.left;
            root = redBlackTransplant(root, val, val.left);
        } else {
            y = findSmallestDescendent(val.right);
            yOriginalColor = y.color;
            x = y.right;
            if (y.parent.val === val.val) {
                x.parent = y;
            } else {
                root = redBlackTransplant(root, y, y.right);
                y.right = val.right;
                y.right.parent = y;
            }
            root = redBlackTransplant(root, val, y);
            y.left = val.left;
            y.left.parent = y;
            y.color = val.color;
        }
        if (yOriginalColor === BLACK) {
            root = redBlackDeleteFixup(root, x);
        }
    }
    return root;
};

exports.deleteValue = deleteValue;

const redBlackDeleteFixup = (root, node) => {
    // maintain Red-black properties of the tree on a node deletion.
    while (node !== NIL && node.val !== root.val && node.color === BLACK) {
        if (node.val === node.parent.left.val) {
            let tmp = node.parent.right;
            if (tmp.color === RED) {
                tmp.color = BLACK;
                node.parent.color = RED;
                root = leftRotate(root, node.parent);
                tmp = node.parent;
            }
            if (tmp.left.color === BLACK && tmp.right.color === BLACK) {
                tmp.color = RED;
                node = node.parent;
            } else {
                if(tmp.right.color === BLACK) {
                    tmp.left.color = BLACK;
                    tmp.color = RED;
                    root = rightRotate(root, tmp);
                    tmp = node.parent.right;
                }
                tmp.color = node.parent.color;
                node.parent.color = BLACK;
                tmp.right.color = BLACK;
                root = leftRotate(root, node.parent);
                node = root;
            }
        } else {
            let tmp = node.parent.left;
            if (tmp.color === RED) {
                tmp.color = BLACK;
                node.parent.color = RED;
                root = rightRotate(root, node.parent);
                tmp = node.parent;
            }
            if (tmp.right.color === BLACK && tmp.left.color === BLACK) {
                tmp.color = RED;
                node = node.parent;
            } else {
                if(tmp.left.color === BLACK) {
                    tmp.right.color = BLACK;
                    tmp.color = RED;
                    root = leftRotate(root, tmp);
                    tmp = node.parent.left;
                }
                tmp.color = node.parent.color;
                node.parent.color = BLACK;
                tmp.left.color = BLACK;
                root = rightRotate(root, node.parent);
                node = root;
            }
        }
    }
    node.color = BLACK;
    return root;
};