require('babel-core/register'); // babel/register does not work on the current file, so the code below is not ES6.

var rbt = require('./red-black.js');

var root = rbt.createNode(""); // initialize tree with a dummy root value

var readline = require('readline');
var fileName = "big.txt";


function sanitizeString(str) {
    // remove basic punctations
    return str.toLocaleLowerCase().replace(/[.,\/#!$%\^&\*;:{}=_`~()\[\]'\?\!\-\"]/g," ").replace(/\s+/g, " ").trim();
}

var fileReader = readline.createInterface({
    input: require('fs').createReadStream(fileName)
});

var consoleReader = readline.createInterface(process.stdin, process.stdout);

console.log("Reading", fileName, "into the Red Black Tree ... ");

fileReader.on('line', function (line) {
    sanitizeString(line).split(" ").forEach(function(word) {
        root = rbt.insertValue(root, word);
    });
});

fileReader.on('close', function() {
    console.log("Read " + fileName + " into the Red-Black Tree successfully!\n");
    console.log("Type the word to lookup in the tree. Press Cntrl-C to exit");
    consoleReader.on('line', function(line) {
        var result = rbt.findValue(root, line.toLocaleLowerCase().trim());
        if (result) {
            console.log(result.val, "found", result.count, "times!");
        } else {
            console.log("No results found");
        }
    }).on('close',function(){
        process.exit(0);
    });
    
});