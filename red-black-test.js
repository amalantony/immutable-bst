"use strict";

require('babel-core/register');

const expect = require('expect');
let deepFreeze = require('deep-freeze');
deepFreeze = () => {};

const rbt = require('./red-black.js');

const validateRBT = (root, print) => {
    /* 
        This function tests for the validity of the passed Red Black Tree:
        
        Conditions to evaluate: 
        - The passed RBT is a valid BST *
        - Root is always Black *
        - Every node is either Red or Black *
        - Every leaf (NIL) is Black *
        - If a node is Red, both it's children are Black
        - For each node, all simple paths from the node to the descendent leaves contain the same number of black nodes
    */

    if (typeof print === "undefined") print = true;

    if(print === true) console.log("\nAssertion: Root of the Red Black Tree is coloured Black --> ");
    expect(root.color).toEqual("BLACK");
    if(print === true) console.log("Success: Root is black!\n");

    if(print === true) console.log("Assertion: Each node must be coloured either RED or BLACK -->");
    if(print === true) console.log("Assertion: All leaf nodes (NIL) must be coloured BLACK -->");
    if(print === true) console.log("Assertion: Each RED node must have both children as BLACK -->");

    const values = [];
    (function walkTreeInOrder(x) {
        if (x !== rbt.NIL) {
            walkTreeInOrder(x.left);
            values.push(x.val);
            expect(x.color).toMatch(/BLACK|RED/);
            if (x === rbt.NIL) {
                expect(x.color).toEqual("BLACK");
            }
            if (x.color === "RED") {
                expect(x.left.color).toEqual("BLACK");
                expect(x.right.color).toEqual("BLACK");
            }
            walkTreeInOrder(x.right);
        }
    })(root);
    
    if(print === true) console.log("---");
    if(print === true) console.log("Success: Each node is coloured either RED or BLACK!");
    if(print === true) console.log("Success: All leaf nodes (NIL) are coloured BLACK!");
    if(print === true) console.log("Sucess: Each RED node has both it's children as BLACK!\n");

    if(print === true) console.log("Assertion: The given Red Black Tree must be a valid Binary Search Tree -->");
    expect(values.slice().sort((a, b) => a - b )).toEqual(values);
    if(print === true) console.log("Success: The given Red Black Tree is a valid Binary Search Tree!\n");

    if(print === true) console.log("Assertion: Each path from Root to the leaves have the same number of Black Nodes --> ");
    let prevCnt = -1;
    function allPaths(node, pathSoFar) {
        if (node.val === "NIL") {
            let cnt = pathSoFar.split(":").reduce((cnt, el) => {
                if(el === "BLACK") {
                    return cnt + 1;
                } else {
                    return cnt;
                }
            }, 0);
            if (prevCnt === -1) {
                prevCnt = cnt;
            }
            expect(cnt).toEqual(prevCnt);
            return;
        }
        allPaths(node.left, pathSoFar + ":" + node.color);
        allPaths(node.right, pathSoFar + ":" + node.color);
    }
    allPaths(root, "");
    if(print === true) console.log("Success: Every path from root to leaves contains the same number of Black Nodes!\n");
};

console.log("\nRUNNING TEST ASSERTIONS FOR RED BLACK TREES ==> ");
console.log("============================================");

let root = rbt.createNode(11);
deepFreeze(root);
root = rbt.insertValue(root, 2);
deepFreeze(root);
root = rbt.insertValue(root, 14);
deepFreeze(root);
root = rbt.insertValue(root, 5);
deepFreeze(root);
root = rbt.insertValue(root, 8);
deepFreeze(root);
root = rbt.insertValue(root, 7);
deepFreeze(root);
root = rbt.insertValue(root, 1);
deepFreeze(root);
root = rbt.insertValue(root, 15);
deepFreeze(root);
root = rbt.insertValue(root, 4);
deepFreeze(root);
root = rbt.insertValue(root, 3);
deepFreeze(root);
root = rbt.insertValue(root, 16);
deepFreeze(root);
root = rbt.insertValue(root, 6);
deepFreeze(root);

validateRBT(root);

console.log("Assertion: Duplicate elements must increment count by not mutating original element");

root = rbt.insertValue(root, 7);
deepFreeze(root);

root = rbt.insertValue(root, 7);
deepFreeze(root);

root = rbt.insertValue(root, 5);
deepFreeze(root);

expect(rbt.findValue(root, 5).element.count).toEqual(2);
expect(rbt.findValue(root, 7).element.count).toEqual(3);

console.log("Suceess: Duplicate elements increments count immutably!\n");


console.log("Assertion: Deletions must be successful");

expect(rbt.findValue(root, 5).found).toNotEqual(false);
root = rbt.deleteValue(root, 5);
validateRBT(root, false); // passing false will supress the messages
expect(rbt.findValue(root, 5).found).toEqual(false);

expect(rbt.findValue(root, 7).found).toNotEqual(false);
root = rbt.deleteValue(root, 7);
validateRBT(root, false); // passing false will supress the messages
expect(rbt.findValue(root, 7).found).toEqual(false);

expect(rbt.findValue(root, 16).found).toNotEqual(false);
root = rbt.deleteValue(root, 16);
validateRBT(root, false); // passing false will supress the messages
expect(rbt.findValue(root, 16).found).toEqual(false);

expect(rbt.findValue(root, 3).found).toNotEqual(false);
root = rbt.deleteValue(root, 3);
validateRBT(root, false); // passing false will supress the messages
expect(rbt.findValue(root, 3).found).toEqual(false);


console.log("Sucess: Deletions successful!\n");

console.log("Assertion: Deletion of a non exisiting node must be handled gracefully");

expect(rbt.findValue(root, 9).found).toEqual(false);
root = rbt.deleteValue(root, 9);
validateRBT(root, false); // passing false will supress the messages
console.log("Sucess: Deletions of a non existing node returns the original root!\n");

console.log("Assertion: Deletion of a NIL node must return a NIL tree");
root = rbt.deleteValue(rbt.NIL, 10);
expect(root).toEqual(rbt.NIL);
console.log("Sucess: Deletions of a NIL tree returns a NIL tree!\n");


console.log("All tests passed! :) \n");
