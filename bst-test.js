"use strict";

require('babel-core/register');

const expect = require('expect');
const deepFreeze = require('deep-freeze');

const bst = require('./bst');

let root = bst.createNode(15);
deepFreeze(root); //don't allow mutations to root

console.log("\nRUNNING ASSERTIONS ---> \n");

console.log("Testing immutable Insertions ... ");

root = bst.insertValue(root, 5);
deepFreeze(root);

root = bst.insertValue(root, 20);
deepFreeze(root);

root = bst.insertValue(root, 8);
deepFreeze(root);

root = bst.insertValue(root, 16);
deepFreeze(root);

root = bst.insertValue(root, 24);
deepFreeze(root);

root = bst.insertValue(root, 17);
deepFreeze(root);

root = bst.insertValue(root, 22);
deepFreeze(root);

root = bst.insertValue(root, 23);
deepFreeze(root);

expect(root).toEqual({
    val: 15,
    left: { 
        val: 5,
        left: null,
        right: { val: 8, left: null, right: null } },
    right: { 
        val: 20,
        left: { val: 16, left: null, right: {val: 17, left: null, right: null} },
        right: { val: 24, left: {val: 22, left: null, right: {val: 23, left: null, right: null}}, right: null } }
});

console.log("Immutable insertions successful! \n");

console.log("Testing lookups ...");

expect(bst.findValue(root, 15)).toEqual(true);
expect(bst.findValue(root, 14)).toEqual(false); 
deepFreeze(root);

console.log("Lookups successful! \n");

// delete node with 2 children
console.log("Testing immutable deletions for a node with 2 children ...");

let newRoot = bst.deleteValue(root, 20);
deepFreeze(newRoot);

expect(newRoot).toEqual({
    val: 15,
    left: { 
        val: 5,
        left: null,
        right: { val: 8, left: null, right: null } },
    right: {
        val: 22,
        left: { val: 16, left: null, right: { val: 17, left: null, right: null } },
        right: { val: 24, left: { val: 23, left: null, right: null }, right: null }}
});


// delete node with no children
console.log("Testing immutable deletions for a node with no children ...");
newRoot = bst.deleteValue(newRoot, 23);
deepFreeze(newRoot);

expect(newRoot).toEqual({
    val: 15,
    left: { 
        val: 5,
        left: null,
        right: { val: 8, left: null, right: null } },
    right: {
        val: 22,
        left: { val: 16, left: null, right: { val: 17, left: null, right: null } },
        right: { val: 24, left: null, right: null }}
});


newRoot = bst.deleteValue(newRoot, 17);
deepFreeze(newRoot);

expect(newRoot).toEqual({
    val: 15,
    left: {
        val: 5,
        left: null,
        right: { val: 8, left: null, right: null } },
    right: {
        val: 22,
        left: { val: 16, left: null, right: null },
        right: { val: 24, left: null, right: null }}
});

// delete nodes with 1 child each

// delete node with 1 right child
console.log("Testing immutable deletions for a node with 1 right child ...");
newRoot = bst.deleteValue(newRoot, 5);
deepFreeze(newRoot);

expect(newRoot).toEqual({
    val: 15,
    left: {
        val: 8,
        left: null,
        right: null 
    },
    right: {
        val: 22,
        left: { val: 16, left: null, right: null },
        right: { val: 24, left: null, right: null }}
});

newRoot = bst.insertValue(newRoot, 23);
deepFreeze(newRoot);

// delete node with 1 left child
console.log("Testing immutable deletions for a node with 1 left child ...");
newRoot = bst.deleteValue(newRoot, 24);
deepFreeze(newRoot);

expect(newRoot).toEqual({
    val: 15,
    left: {
        val: 8,
        left: null,
        right: null 
    },
    right: {
        val: 22,
        left: { val: 16, left: null, right: null },
        right: { val: 23, left: null, right: null }}
});

console.log("Immutable deletions successful! \n")

// delete a non existent value
console.log("Deleting a non existent element ...");
newRoot = bst.deleteValue(newRoot, 30);
expect(newRoot).toEqual({
    val: 15,
    left: {
        val: 8,
        left: null,
        right: null 
    },
    right: {
        val: 22,
        left: { val: 16, left: null, right: null },
        right: { val: 23, left: null, right: null }}
});
console.log("Success: Deleting a non existent element returns the original tree back as expected!\n");

// delete from a null tree
console.log("Deleting from a null tree ...");

newRoot = bst.deleteValue(null, 10);
expect(newRoot).toEqual(null);

console.log("Success: Deletion from a null tree returned a null tree back as expected! \n");

console.log("All tests passed! :)\n");