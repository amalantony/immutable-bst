const createNode = (val, left = null, right = null) => {
    return {
        val: val,
        left: left,
        right: right
    };
};

exports.createNode = createNode;

const insertValue = (root, num, comparator = (a, b) => a > b) => {
    let resultRoot = null, cloneRoot = null, direction = "right";

    let tmp = null, parent = null; // variables to traverse main tree
    let cloneTmp = null, cloneParent = null; // variables to travese clone tree

    if (root === null) {
        resultRoot = createNode(num);
    } else {
        cloneRoot = createNode(root.val);
        tmp = root;
        cloneTmp = cloneRoot;
        while (tmp != null) {
            parent = tmp;
            cloneParent = cloneTmp;   
            if(comparator(tmp.val, num)) {
                cloneParent.right = parent.right;
                if (parent.left != null) cloneParent.left = createNode(parent.left.val);
                tmp = tmp.left;
                cloneTmp = cloneTmp.left;
                direction = "left";
            } else {
                cloneParent.left = parent.left;
                if (parent.right != null) cloneParent.right = createNode(parent.right.val);
                tmp = tmp.right;
                cloneTmp = cloneTmp.right;
                direction = "right";
            }
        }
        if (direction == "left") {
            cloneParent.left = createNode(num);
        } else {
            cloneParent.right = createNode(num);
        }
        resultRoot = cloneRoot;
    }
    return resultRoot;
};

exports.insertValue = insertValue;

const findValue = (root, num) => {
    let tmp = root, found = false;
    while(tmp != null) {
        if (tmp.val === num) {
            found = true;
            break;
        } else if (tmp.val >= num) {
            tmp = tmp.left;
        } else {
            tmp = tmp.right;
        }
    }
    return found;
};

exports.findValue = findValue;

const findSmallestDescendent = (node) => {
    // find the smallest descendent of node (useful for deletions)
    let tmp = node, self = null, parent = null;
    while (tmp!= null) {
        parent = self;
        self = tmp;
        tmp = tmp.left;
    }
    return {self, parent};
};

exports.findSmallestDescendent = findSmallestDescendent;

const deleteValue = (root, num, comparator = (a, b) => a > b) => {
    // see if node to be deleted has 0, 1 or 2 children
    let tmp = root, found = false, parent = null, direction = null;
    let cloneTmp = null, cloneParent = null, cloneRoot = null; // variables to track the clone BST
    if(root) cloneRoot = createNode(root.val);
    cloneTmp = cloneRoot;
    while (tmp != null) {
        if (tmp.val === num) {
            found = true;
            break;
        } else if (comparator(tmp.val, num)) {
            cloneParent = cloneTmp;
            parent = tmp;
            cloneParent.right = parent.right;
            if (parent.left != null) cloneParent.left = createNode(parent.left.val);
            cloneTmp = cloneTmp.left;
            tmp = tmp.left;
            direction = "left";
        } else {
            cloneParent = cloneTmp;
            parent = tmp;
            cloneParent.left = parent.left;
            if (parent.right != null) cloneParent.right = createNode(parent.right.val);
            cloneTmp = cloneTmp.right;
            tmp = tmp.right;
            direction = "right";
        }
    }
    if (found === true) {
        //use tmp and parent pointers to delete node in question.
        if (tmp.left === null && tmp.right === null) {
            // no children
            if (parent.val <= tmp.val) {
                cloneParent.right = null;
            } else {
                cloneParent.left = null;
            }
        } else if (tmp.left === null || tmp.right === null) {
            // 1 child
            if (tmp.left === null) {
                if (direction === "left") {
                    cloneParent.left = tmp.right;
                } else {
                    cloneParent.right = tmp.right;
                }
            } else {
                if (direction === "left") {
                    cloneParent.left = tmp.left;
                } else {
                    cloneParent.right = tmp.left;
                }
            }
        } else {
            // 2 children
            let result = findSmallestDescendent(tmp.right);
            let smallestDescendent = result.self;
            let parent = result.parent;
            cloneTmp.val = smallestDescendent.val;
            cloneTmp.right = tmp.right;
            cloneTmp.left = tmp.left;
            let newSubTree = deleteValue(cloneTmp[direction], cloneTmp.val);
            cloneTmp[direction] = newSubTree;
        }
    }
    return cloneRoot;
};

exports.deleteValue = deleteValue;
